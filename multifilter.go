// Copyright (c) 2020 Doc.ai and/or its affiliates.
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package multifilter

import (
	"context"
	"crypto/tls"
	"time"

	"github.com/coredns/coredns/plugin"
	"github.com/coredns/coredns/plugin/debug"
	"github.com/coredns/coredns/plugin/dnstap"
	"github.com/coredns/coredns/plugin/metadata"
	clog "github.com/coredns/coredns/plugin/pkg/log"
	"github.com/coredns/coredns/request"
	"github.com/miekg/dns"
	"github.com/pkg/errors"
)

var log = clog.NewWithPlugin("multifilter")

// MultiFilter represents a plugin instance that can do async requests to list of DNS servers.
type MultiFilter struct {
	clients        []Client
	tlsConfig      *tls.Config
	excludeDomains Domain
	filteredIps    []string
	setSoa         string
	tlsServerName  string
	timeout        time.Duration
	race           bool
	net            string
	from           string
	attempts       int
	workerCount    int
	tapPlugin      *dnstap.Dnstap
	Next           plugin.Handler
}

// New returns reference to new MultiFilter plugin instance with default configs.
func New() *MultiFilter {
	return &MultiFilter{
		tlsConfig:      new(tls.Config),
		net:            "udp",
		attempts:       3,
		timeout:        defaultTimeout,
		excludeDomains: NewDomain(),
		setSoa:         "filtered.dns.local.",
	}
}

func (f *MultiFilter) addClient(p Client) {
	f.clients = append(f.clients, p)
	f.workerCount++
}

// Name implements plugin.Handler.
func (f *MultiFilter) Name() string {
	return "multifilter"
}

// ServeDNS implements plugin.Handler.
func (f *MultiFilter) ServeDNS(ctx context.Context, w dns.ResponseWriter, m *dns.Msg) (int, error) {
	req := request.Request{W: w, Req: m}
	if !f.match(&req) {
		return plugin.NextOrFailure(f.Name(), f.Next, ctx, w, m)
	}

	timeoutContext, cancel := context.WithTimeout(ctx, f.timeout)
	defer cancel()
	clientCount := len(f.clients)
	workerChannel := make(chan Client, f.workerCount)
	responseCh := make(chan *response, clientCount)

	go func() {
		defer close(workerChannel)
		for i := 0; i < clientCount; i++ {
			client := f.clients[i]
			select {
			case <-timeoutContext.Done():
				return
			case workerChannel <- client:
				continue
			}
		}
	}()

	for i := 0; i < f.workerCount; i++ {
		go func() {
			for c := range workerChannel {
				responseCh <- f.processClient(timeoutContext, c, &request.Request{W: w, Req: m})
			}
		}()
	}

	result := f.getMultiFilterResult(timeoutContext, req, responseCh)
	if result == nil {
		return plugin.NextOrFailure(f.Name(), f.Next, ctx, w, m)
	}

	metadata.SetValueFunc(ctx, "multifilter/upstream", func() string {
		return result.client.Endpoint()
	})
	if result.err != nil {
		return dns.RcodeServerFailure, result.err
	}

	if f.tapPlugin != nil {
		toDnstap(f, result.client.Endpoint(), &req, result.response, result.start)
	}

	if !req.Match(result.response) {
		debug.Hexdumpf(result.response, "Wrong reply for id: %d, %s %d", result.response.Id, req.QName(), req.QType())
		formerr := new(dns.Msg)
		formerr.SetRcode(req.Req, dns.RcodeFormatError)
		logErrIfNotNil(w.WriteMsg(formerr))
		return 0, nil
	}

	logErrIfNotNil(w.WriteMsg(result.response))
	return 0, nil
}

func (f *MultiFilter) getMultiFilterResult(ctx context.Context, req request.Request, responseCh <-chan *response) *response {
	count := len(f.clients)

	for {
		select {
		case <-ctx.Done():
			return nil
		case r := <-responseCh:
			count--

			// CleanBrowsing returns NXDOMAIN
			if r.response.Rcode == dns.RcodeNameError {
				log.Infof("(%s) NXDOMAIN", r.client.Endpoint())
				return createNXDomainResponse(r, req, f.setSoa)
			}

			// OpenDNS & Cloudflare return NOERROR
			if r.response.Rcode == dns.RcodeSuccess {
				for _, rr := range r.response.Answer {
					a, ok := rr.(*dns.A)

					if !ok {
						return nil
					}

					// Cloudflare sets the IP to 0.0.0.0
					if a.A.String() == "0.0.0.0" {
						log.Infof("(%s) %s", r.client.Endpoint(), a.A.String())
						return createNXDomainResponse(r, req, f.setSoa)
					}

					// OpenDNS sets the IP to their servers
					for _, ip := range f.filteredIps {
						if a.A.String() == ip {
							log.Infof("(%s) %s", r.client.Endpoint(), a.A.String())
							return createNXDomainResponse(r, req, f.setSoa)
						}
					}
				}
			}

			if count == 0 {
				return nil
			}
		}
	}
}

func (f *MultiFilter) match(state *request.Request) bool {
	if !plugin.Name(f.from).Matches(state.Name()) || f.excludeDomains.Contains(state.Name()) {
		return false
	}
	return true
}

func (f *MultiFilter) processClient(ctx context.Context, c Client, r *request.Request) *response {
	start := time.Now()
	var err error
	for j := 0; j < f.attempts || f.attempts == 0; <-time.After(attemptDelay) {
		if ctx.Err() != nil {
			return &response{client: c, response: nil, start: start, err: ctx.Err()}
		}
		var msg *dns.Msg
		msg, err = c.Request(ctx, r)
		if err == nil {
			return &response{client: c, response: msg, start: start, err: err}
		}
		if f.attempts != 0 {
			j++
		}
	}
	return &response{client: c, response: nil, start: start, err: errors.Wrapf(err, "attempt limit has been reached")}
}

func createNXDomainResponse(r *response, req request.Request, soa string) *response {
	newResponse := &dns.Msg{}
	newResponse.SetReply(req.Req)
	newResponse.Authoritative = true
	newResponse.Rcode = dns.RcodeNameError
	newResponse.Ns = append(newResponse.Ns, &dns.SOA{
		Hdr: dns.RR_Header{
			Name:   soa,
			Rrtype: dns.TypeSOA,
			Class:  dns.ClassINET,
			Ttl:    86400,
		},
		Ns:      soa,
		Mbox:    soa,
		Serial:  1,
		Refresh: 7200,
		Retry:   900,
		Expire:  1209600,
		Minttl:  86400,
	})
	return &response{client: r.client, response: newResponse, start: r.start, err: nil}
}

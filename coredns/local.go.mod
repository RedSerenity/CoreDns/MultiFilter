module gitlab.com/RedSerenity/CoreDns/MultiFilter/coredns

go 1.22

toolchain go1.22.1

require (
	github.com/coredns/coredns v1.11.2
	gitlab.com/RedSerenity/CoreDns/MultiFilter v1.0.0
)

replace gitlab.com/RedSerenity/CoreDns/MultiFilter => ../

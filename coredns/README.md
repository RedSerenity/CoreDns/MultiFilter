## Build CoreDns with MultiFilter Plugin

There are two ways to build CoreDns with additional plugins enabled.

### Build via Coredns Source Code

```bash
$ cd $GOPATH
$ git clone https://github.com/coredns/coredns
$ cd coredns
```
Add `multifilter:gitlab.com/RedSerenity/CoreDns/MultiFilter` just above `forward:forward` in the `plugin.cfg` file.

```bash
$ make
```

### Build via Custom `main.go` File

```bash
$ go build -o coredns/coredns coredns/main.go
```

or

```bash
$ docker build -t "${ORG}/coredns:${TAG}" .
```
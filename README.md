# Multi Filter

## Name

*multi-filter* - query multiple dns filtering services simultaneously and return the first filtered response.

## Description

Based upon the _fanout_ plugin, this plugin queries multiple dns filtering services in parallel and then if any of the results are filtered, it returns NXDOMAIN. This allows you to use multiple dns filtering services to filter pornography, malicious sites, etc.

## Syntax

The syntax is nearly identical to _fanout_ but with a few small changes.

* `set-soa` defines the SOA name to use when a result is filtered. Defaults to `filtered.dns.local`.
* `filtered-ips` is a space seperated list of ips that signal a domain has been filtered. For example, while CleanBrowsing returns an NXDOMAIN for filtered domains, OpenDNS returns an A record with 146.112.61.106 that shows a blocked message in a browser. `filtered-ips` allows multifilter to know when to return NXDOMAIN.

* `worker-count` is the number of parallel queries per request. By default equals to count of IP list. Use this only for reducing parallel queries per request.
* `network` is a specific network protocol. Could be `tcp`, `udp`, `tcp-tls`.
* `except` is a list is a space-separated list of domains to exclude from proxying.
* `except-file` is the path to file with line-separated list of domains to exclude from proxying.
* `attempt-count` is the number of attempts to connect to upstream servers that are needed before considering an upstream to be down. If 0, the upstream will never be marked as down and request will be finished by `timeout`. Default is `3`.
* `timeout` is the timeout of request. After this period, attempts to receive a response from the upstream servers will be stopped. Default is `30s`.

* `tls` **CERT** **KEY** **CA** define the TLS properties for TLS connection. From 0 to 3 arguments can be
  provided with the meaning as described below
  * `tls` - no client authentication is used, and the system CAs are used to verify the server certificate
  * `tls` **CA** - no client authentication is used, and the file CA is used to verify the server certificate
  * `tls` **CERT** **KEY** - client authentication is used with the specified cert/key pair.
    The server certificate is verified with the system CAs
  * `tls` **CERT** **KEY**  **CA** - client authentication is used with the specified cert/key pair.
    The server certificate is verified using the specified CA file
* `tls_server` **NAME** allows you to set a server name in the TLS configuration; for instance 9.9.9.9
  needs this to be set to `dns.quad9.net`. Multiple upstreams are still allowed in this scenario,
  but they have to use the same `tls_server`. E.g. mixing 9.9.9.9 (QuadDNS) with 1.1.1.1
  (Cloudflare) will not work.

## Metrics

If monitoring is enabled (via the *prometheus* plugin) then the following metric are exported:

* `coredns_multifilter_request_duration_seconds{to}` - duration per upstream interaction.
* `coredns_multifilter_request_count_total{to}` - query count per upstream.
* `coredns_multifilter_response_rcode_count_total{to, rcode}` - count of RCODEs per upstream.

Where `to` is one of the upstream servers (**TO** from the config), `rcode` is the returned RCODE
from the upstream.

## Examples

Filter dns queries through CleanBrowsing, OpenDns and Cloudflare.  The first block response from a filter will be provided as the result. If the domain is not blocked, use the forward plugin to lookup the actual ip. Set the `filtered-ips` so that OpenDNS works without their "You've been blocked" redirect.

~~~ corefile
. {
  multifilter . 185.228.168.168:53 208.67.222.123:53 1.1.1.3:53 {
    filtered-ips 146.112.61.106
  }

  forward . 8.8.8.8
}
~~~

Set the SOA record

~~~ corefile
. {
  multifilter . 185.228.168.168:53 208.67.222.123:53 1.1.1.3:53 {
    filtered-ips 146.112.61.106
    
    set-soa youve-been.filtered.local
  }

  forward . 8.8.8.8
}
~~~

Filter everything except requests to `example.org`

~~~ corefile
. {
  multifilter . 185.228.168.168:53 208.67.222.123:53 1.1.1.3:53 {
    filtered-ips 146.112.61.106
    
    except example.org
  }

  forward . 8.8.8.8
}
~~~

For more examples on using other options, such as tls, see the documentation for [fanout](https://coredns.io/explugins/fanout/).